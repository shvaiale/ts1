# Software testing repository

---

## Repository information

---

This repostory is created to track progress 
in **[Software testing](https://intranet.fel.cvut.cz/en/education/bk/predmety/31/30/p3130006.html)** 
CTU course and obtain practice with git projects. It contains code from classes 
and homework.


## In-Class work

---

<details>
  <summary><b> Lab 1 </b></summary>
    <p>
    Getting basic information about maven projects, JUnit framework and git. Writing
    one simple test case. Creating a git repository.
    </p>
</details>

<details>
  <summary><b> Lab 2 </b></summary>
    <p>
    An introduction to the terms "Equivalence classes" and "Boundary values".
    </p>
</details>

<details>
  <summary><b> Lab 3 </b></summary>
    <p>
    An introduction to the basics of JUnit.
    </p>
</details>

<details>
  <summary><b> Lab 4 </b></summary>
    <p>
    Code refactoring and testing.
    </p>
</details>

<details>
  <summary><b> Lab 5 </b></summary>
    <p>
    Practice in mocking.
    </p>
</details>

<details>
  <summary><b> Lab 6 </b></summary>
    <p>
    Pairwise testing.
    </p>
</details>

<details>
  <summary><b> Lab 7 </b></summary>
    <p>
    Process testing.
    </p>
</details>

<details>
  <summary><b> Lab 9 </b></summary>
    <p>
    Selenium basics.
    </p>
</details>

<details>
  <summary><b> Lab 10 </b></summary>
    <p>
    Selenium Page Object Model.
    </p>
</details>

<details>
  <summary><b> Lab 11 </b></summary>
    <p>
    Selenium process automatization. "Test for Robot".
    </p>
</details>

## Homework assignments

---

<details>
  <summary><b> Homework 1 </b></summary>
    <p>
    Practice in teamwork with git. Assignment:
    </p>
    <p>
    Student 1: Add a colleague as a Maintainer. Don't forget to add your teacher to the project as well.
    </p>
    <p>
    Student 2: Clone the project you were added to. Create a new branch in it.
    Add a method that calculates the factorial. Push the new branch.
    </p>
    <p>
    Student 1: After your colleague pushes the new branch, switch to it.
    Add the factorial method test(s) to the project and upload to the repository.
    Merge all changes into the main branch.
    </p>
</details>

<details>
  <summary><b> Homework 2 </b></summary>
    <p>
    Practice with JUnit basics. Assignment:
    </p>
    <p>
    In the created calculator project, create a CalculatorTest class and add one test for each calculator method.
    </p>
    <p>
    Also create a test for the case where zero is inserted in the denominator of the division method. Use an assert that expects an exception to be thrown.
    </p>
    <p>
    Then push the finished calculator maven project with the test class to your remote repository.
    </p>
</details>

<details>
  <summary><b> Homework 3 </b></summary>
    <p>
    Equivalence classes. Assignment: test on Moodle.
    </p>
</details>

<details>
  <summary><b> Homework 4 </b></summary>
    <p>
    Big assignment consisting of practice in Mockito, JUnit, integration testing.
    </p>
    <p>
    Assignment: test classes of eshop.
    </p>
</details>

<details>
  <summary><b> Homework 5 </b></summary>
    <p>
    Pairwise-testing. Assignment:
    </p>
    <p>
    Assignment: find combinations for pairwise testing of ticket booking system from lab02/lab03.
    </p>
</details>

<details>
  <summary><b> Homework 6 </b></summary>
    <p>
    Process testing.
    </p>
    <p>
    1. Convert the UML activity diagram into an oriented graph with branch points and transitions between them.
    </p>
    <p>
    2. Manually generate the passes in the process test application with TDL=1 and TDL=2 coverage.
    </p>
    <p>
    3. Use Oxygen to generate processes with TDL=3.
    </p>
</details>

<details>
  <summary><b> Homework 7 </b></summary>
    <p>
    Selenium testing.
    </p>
    <p>
    Test logging in and searching by title on website https://link.springer.com/
    </p>
</details>