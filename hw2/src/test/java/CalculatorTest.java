import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CalculatorTest {

    @Test
    void add_adds3and4_returns7() {
        int actual = new Calculator().add(3, 4);
        int expected = 7;

        Assertions.assertEquals(expected, actual);
    }

    @Test
    void subtract_subtracts5and1_returns4() {
        int actual = new Calculator().subtract(5, 1);
        int expected = 4;

        Assertions.assertEquals(expected, actual);
    }

    @Test
    void multiply_multiplies5and2_returns10() {
        int actual = new Calculator().multiply(5, 2);
        int expected = 10;

        Assertions.assertEquals(expected, actual);
    }

    @Test
    void divide_divides5and2_returns2() {
        int actual = new Calculator().divide(5, 2);
        int expected = 2;

        Assertions.assertEquals(expected, actual);
    }

    @Test
    void divide_divides5and0_throwsException() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> new Calculator().divide(5, 0));
    }
}