package cz.cvut.fel.ts1.archive;

import cz.cvut.fel.ts1.shop.Item;
import cz.cvut.fel.ts1.shop.Order;
import cz.cvut.fel.ts1.shop.StandardItem;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.times;

class PurchasesArchiveTest {

    private static PurchasesArchive createArchiveWithMockedEntryForPrint() {
        ItemPurchaseArchiveEntry entry = Mockito.mock(ItemPurchaseArchiveEntry.class);
        Mockito.when(entry.toString()).thenReturn("mocked");
        HashMap<Integer, ItemPurchaseArchiveEntry> map = new HashMap<>();
        map.put(1, entry);
        return new PurchasesArchive(map, new ArrayList<>());
    }

    @Test
    void printItemPurchaseStatistics_PrintToRedirectedOut_PrintsProperly() {
        // Redirect output
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        PrintStream originalOut = System.out;
        System.setOut(new PrintStream(outputStream));
        // Create archive with one mocked entry
        PurchasesArchive archive = createArchiveWithMockedEntryForPrint();

        archive.printItemPurchaseStatistics();

        assertEquals("""
                        ITEM PURCHASE STATISTICS:\r
                        mocked\r
                        """,
                outputStream.toString());

        // Return output
        System.setOut(originalOut);
    }

    @Test
    void getHowManyTimesHasBeenItemSold_ArchiveDoesNotContainItem_ReturnZero() {
        PurchasesArchive archive = new PurchasesArchive();
        Item item = new StandardItem(0, "",0, "", 0);

        int result = archive.getHowManyTimesHasBeenItemSold(item);

        assertEquals(0, result);
    }

    private static PurchasesArchive createArchiveWithMockedEntryForGet() {
        ItemPurchaseArchiveEntry entry = Mockito.mock(ItemPurchaseArchiveEntry.class);
        Mockito.when(entry.getCountHowManyTimesHasBeenSold()).thenReturn(10);
        HashMap<Integer, ItemPurchaseArchiveEntry> map = new HashMap<>();
        map.put(1, entry);
        return new PurchasesArchive(map, new ArrayList<>());
    }

    @Test
    void getHowManyTimesHasBeenItemSold_ArchiveContainsItem_ReturnProperCount() {
        Item item = new StandardItem(1, "",0, "", 0);
        PurchasesArchive archive = createArchiveWithMockedEntryForGet();

        int result = archive.getHowManyTimesHasBeenItemSold(item);

        assertEquals(10, result);
    }

    @Test
    void putOrderToPurchasesArchive_putOrder_AddsToOrderArchive() {
        ArrayList<Order> orderArchive = Mockito.mock(ArrayList.class);
        PurchasesArchive archive = new PurchasesArchive(new HashMap<>(), orderArchive);
        Order order = Mockito.mock(Order.class);

        archive.putOrderToPurchasesArchive(order);

        Mockito.verify(orderArchive).add(order);
    }

    @Test
    void putOrderToPurchasesArchive_ArchiveDoesNotContainItemFromOrder_AddsNewEntry() {
        // Mock order
        ArrayList<Item> items = new ArrayList<>();
        int id = 1;
        items.add(new StandardItem(id, "", 0, "", 0));
        Order order = Mockito.mock(Order.class);
        Mockito.when(order.getItems()).thenReturn(items);
        // Mock archive entries
        HashMap<Integer, ItemPurchaseArchiveEntry> entries = Mockito.mock(HashMap.class);
        Mockito.when(entries.containsKey(id)).thenReturn(false);
        PurchasesArchive archive = new PurchasesArchive(entries, new ArrayList<>());

        archive.putOrderToPurchasesArchive(order);

        Mockito.verify(entries, times(1)).put(eq(id), any(ItemPurchaseArchiveEntry.class));
    }

    @Test
    void putOrderToPurchasesArchive_ArchiveContainsItemFromOrder_IncreaseSoldCount() {
        // Mock order
        ArrayList<Item> items = new ArrayList<>();
        int id = 1;
        items.add(new StandardItem(id, "", 0, "", 0));
        Order order = Mockito.mock(Order.class);
        Mockito.when(order.getItems()).thenReturn(items);
        // Mock entry
        ItemPurchaseArchiveEntry entry = Mockito.mock(ItemPurchaseArchiveEntry.class);
        // Mock archive entries
        HashMap<Integer, ItemPurchaseArchiveEntry> entries = Mockito.mock(HashMap.class);
        Mockito.when(entries.containsKey(id)).thenReturn(true);
        Mockito.when(entries.get(id)).thenReturn(entry);
        PurchasesArchive archive = new PurchasesArchive(entries, new ArrayList<>());

        archive.putOrderToPurchasesArchive(order);

        Mockito.verify(entry, times(1)).increaseCountHowManyTimesHasBeenSold(1);
    }
}
