package cz.cvut.fel.ts1.shop;

import cz.cvut.fel.ts1.archive.PurchasesArchive;
import cz.cvut.fel.ts1.storage.NoItemInStorage;
import cz.cvut.fel.ts1.storage.Storage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;

class EShopControllerTest {

    private static PurchasesArchive archive;
    private static ArrayList<ShoppingCart> carts;
    private static Storage storage;

    private final static int storageItemCount = 5;
    private final static Item storageItem = new StandardItem(1, "Dancing Panda v.2", 5000, "GADGETS", 5);
    private final static Item itemNotInStorage = new StandardItem(2, "Screwdriver", 200, "TOOLS", 5);

    @BeforeEach
    void start() {
        archive = Mockito.mock(PurchasesArchive.class);
        carts = Mockito.mock(ArrayList.class);
        storage = new Storage();
        storage.insertItems(storageItem, storageItemCount);

        EShopController.archive =archive;
        EShopController.carts =carts;
        EShopController.storage = storage;
    }

    @Test
    void CreateCart_addToCart_RemoveFromCart_PurchaseCartWithEmptyStorage_ThrowsException() {
        //Create cart
        ShoppingCart cart = EShopController.newCart();
        Mockito.verify(carts).add(cart);

        //Add items to cart
        cart.addItem(storageItem);
        cart.addItem(itemNotInStorage);
        assertTrue(cart.getCartItems().contains(storageItem));
        assertTrue(cart.getCartItems().contains(itemNotInStorage));

        //Remove item from cart
        cart.removeItem(storageItem.getID());
        assertFalse(cart.getCartItems().contains(storageItem));

        //Purchase cart which contains itemNotInStorage
        assertThrows(NoItemInStorage.class, () -> EShopController.purchaseShoppingCart(cart, "Name", "Address"));
    }

    @Test
    void CreateCart_addToCart_PurchaseCart_StorageAndArchiveProcessProperly() {
        //Create cart
        ShoppingCart cart = EShopController.newCart();
        Mockito.verify(carts).add(cart);

        //Add items to cart
        cart.addItem(storageItem);
        assertTrue(cart.getCartItems().contains(storageItem));

        //Purchase cart which contains item from storage
        try {
            EShopController.purchaseShoppingCart(cart, "Name", "Address");

            // Archive saves information about order
            Mockito.verify(archive, Mockito.times(1)).putOrderToPurchasesArchive(any(Order.class));

            // Storage decreases item count
            assertEquals(storageItemCount - 1, storage.getItemCount(storageItem));

        } catch (NoItemInStorage e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    void CreateCart_addManyToCart_PurchaseCart_StorageHaveTooFewItems_ThrowsException() {
        //Create cart
        ShoppingCart cart = EShopController.newCart();
        Mockito.verify(carts).add(cart);

        //Add items to cart
        for (int i = 0; i < storageItemCount + 1; i++)
            cart.addItem(storageItem);
        assertTrue(cart.getCartItems().contains(storageItem));

        //Purchase cart which contains too many items from storage
        assertThrows(NoItemInStorage.class, () -> {
            EShopController.purchaseShoppingCart(cart, "Name", "Address");
        });
    }
}