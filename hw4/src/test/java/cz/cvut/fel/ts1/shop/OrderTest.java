package cz.cvut.fel.ts1.shop;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class OrderTest {

    @Test
    void firstConstructor_createNormalOrder_AssignsProperly() {
        ShoppingCart cart = new ShoppingCart();
        String customerName = "Name";
        String customerAddress = "Address";
        int state = 10;

        Order order = new Order(cart, customerName, customerAddress, state);

        assertEquals(cart.getCartItems(), order.getItems());
        assertEquals(customerName, order.getCustomerName());
        assertEquals(customerAddress, order.getCustomerAddress());
        assertEquals(state, order.getState());
    }

    @Test
    void secondConstructor_createNormalOrder_AssignsProperly() {
        ShoppingCart cart = new ShoppingCart();
        String customerName = "Name";
        String customerAddress = "Address";

        Order order = new Order(cart, customerName, customerAddress);

        assertEquals(cart.getCartItems(), order.getItems());
        assertEquals(customerName, order.getCustomerName());
        assertEquals(customerAddress, order.getCustomerAddress());
        assertEquals(0, order.getState());
    }

    @Test
    void firstConstructor_createWithNulls_throwsException() {
        assertThrows(NullPointerException.class, () -> {
            new Order(null, null, null, 0);
        });
    }

    @Test
    void secondConstructor_createWithNulls_throwsException() {
        assertThrows(NullPointerException.class, () -> {
            new Order(null, null, null);
        });
    }
}
