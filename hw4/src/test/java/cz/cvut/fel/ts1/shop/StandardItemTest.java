package cz.cvut.fel.ts1.shop;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class StandardItemTest {
    private static final StandardItem staticItem = new StandardItem(5, "Name", 0.5f, "Category", 100);

    @Test
    void constructor_CreateInstance_AssignsProperly() {
        final int id = 5;
        final String name = "Name";
        final float price = 0.5f;
        final String category = "Category";
        final int loyaltyPoints = 100;

        StandardItem item = new StandardItem(id, name, price, category, loyaltyPoints);

        assertEquals(id, item.getID());
        assertEquals(name, item.getName());
        assertEquals(price, item.getPrice());
        assertEquals(category, item.getCategory());
        assertEquals(loyaltyPoints, item.getLoyaltyPoints());
    }

    @Test
    void copy_CopyStandardItem_FieldsAreTheSame() {
        StandardItem copied = staticItem.copy();

        assertEquals(staticItem.getID(), copied.getID());
        assertEquals(staticItem.getName(), copied.getName());
        assertEquals(staticItem.getPrice(), copied.getPrice());
        assertEquals(staticItem.getCategory(), copied.getCategory());
        assertEquals(staticItem.getLoyaltyPoints(), copied.getLoyaltyPoints());
    }

    private static Stream<Arguments> provideItemsForEquals() {
        return Stream.of(
                Arguments.of(staticItem, true),
                Arguments.of(staticItem.copy(), true),
                Arguments.of(new StandardItem(5, "Name", 0.5f, "Category", 100), true),
                Arguments.of(null, false),
                Arguments.of(new StandardItem(5, "Name", 0.5f, "Category", 0), false),
                Arguments.of(new StandardItem(5, "Name", 0.5f, "", 100), false),
                Arguments.of(new StandardItem(5, "Name", 0, "Category", 100), false),
                Arguments.of(new StandardItem(5, "", 0.5f, "Category", 100), false),
                Arguments.of(new StandardItem(0, "Name", 0.5f, "Category", 100), false)
        );
    }

    @ParameterizedTest
    @MethodSource("provideItemsForEquals")
    void equals_CompareToStaticItem_ReturnsExpected(StandardItem item, boolean expected) {
        assertEquals(expected, staticItem.equals(item));
    }
}