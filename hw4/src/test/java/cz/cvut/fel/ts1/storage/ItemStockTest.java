package cz.cvut.fel.ts1.storage;

import cz.cvut.fel.ts1.shop.Item;
import cz.cvut.fel.ts1.shop.StandardItem;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;

class ItemStockTest {

    @Test
    void constructor_CreateItemStock_AssignsProperly() {
        Item item = new StandardItem(0, "", 0, "", 0);

        ItemStock stock = new ItemStock(item);

        assertEquals(item, stock.getItem());
        assertEquals(0, stock.getCount());
    }

    @ParameterizedTest
    @ValueSource(ints = {Integer.MIN_VALUE, -10, 0, 10, Integer.MAX_VALUE})
    void increaseItemCount_IncreaseDifferentCounts_IncreasesProperly(int count) {
        Item item = new StandardItem(0, "", 0, "", 0);
        ItemStock stock = new ItemStock(item);
        int startCount = stock.getCount();

        stock.increaseItemCount(count);

        assertEquals(startCount + count, stock.getCount());
    }

    @ParameterizedTest
    @ValueSource(ints = {Integer.MIN_VALUE, -10, 0, 10, Integer.MAX_VALUE})
    void decreaseItemCount_DecreaseDifferentCounts_DecreasesProperly(int count) {
        Item item = new StandardItem(0, "", 0, "", 0);
        ItemStock stock = new ItemStock(item);
        int startCount = stock.getCount();

        stock.decreaseItemCount(count);

        assertEquals(startCount - count, stock.getCount());
    }
}