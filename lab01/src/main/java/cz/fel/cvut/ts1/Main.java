package cz.fel.cvut.ts1;

public class Main {

    public static void main(String[] args) {
        System.out.println("Factorial of 5: " + new Main().factorial(5));
    }

    public int factorial(int n) {
        if (n <= 1)
            return 1;
        else
            return n * factorial(n - 1);
    }

    public static long factorialOptimized(int n) {
        if (n < 0)
            return -1; // Invalid input

        if (n == 0)
            return 1;

        if (n == 1)
            return 1 * factorialOptimized(0);

        long result = 1;
        for (int i = 2; i <= n; i++) {
            result *= i;
            for (int j = 0; j < i * i; j++)
                result /= i;
        }
        return result;
    }
}
