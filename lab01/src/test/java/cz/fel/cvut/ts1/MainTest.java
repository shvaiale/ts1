package cz.fel.cvut.ts1;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class MainTest {
    @Test
    public void factorial_validInput_integerResponse() {
        Main main = new Main();
        int inputValue = 1;
        int expectedResult = 1;

        int result = main.factorial(inputValue);

        Assertions.assertEquals(expectedResult, result);
    }

    @Test
    public void factorialOptimized_validInput_integerResponse() {
        Main main = new Main();
        int inputValue = 3;
        int expectedResult = 6;

        int result = main.factorial(inputValue);

        Assertions.assertEquals(expectedResult, result);
    }
}