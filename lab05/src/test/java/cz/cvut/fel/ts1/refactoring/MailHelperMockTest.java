package cz.cvut.fel.ts1.refactoring;

import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.*;

public class MailHelperMockTest {

    @Test
    public void createAndSendMail_saveMail_callOnce() {
        DBManager mockedManager = mock(DBManager.class);
        MailHelper helper = new MailHelper(mockedManager);
        when(mockedManager.findMail(anyInt())).thenReturn(null);

        helper.sendMail(1);

        verify(mockedManager, times(1)).saveMail(any(Mail.class));

    }
}
