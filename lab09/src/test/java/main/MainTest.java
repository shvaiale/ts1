package main;

import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.support.ui.Select;

import static org.junit.jupiter.api.Assertions.*;

class MainTest {
    @Test
    void sendForm_NotFilledFields_URLNotChanged() {
        WebDriver driver = new EdgeDriver();
        String startURL = "https://ts1.v-sources.eu/";

        driver.get(startURL);
        driver.findElement(By.id("flight_form_submit")).click();

        assertEquals(startURL, driver.getCurrentUrl());
    }

    @Test
    void sendForm_FilledFields_URLChanged() {
        WebDriver driver = new EdgeDriver();
        String startURL = "https://ts1.v-sources.eu/";
        String expectedURL = "https://ts1.v-sources.eu/done";

        driver.get(startURL);
        driver.findElement(By.id("flight_form_firstName")).sendKeys("Ales");
        driver.findElement(By.id("flight_form_lastName")).sendKeys("Shvaibovich");
        driver.findElement(By.id("flight_form_email")).sendKeys("neironis94@gmail.com");
        driver.findElement(By.id("flight_form_birthDate")).sendKeys("11.07.2003");
        Select selectDestination = new Select(driver.findElement(By.id("flight_form_destination")));
        selectDestination.selectByVisibleText("Paris");
        driver.findElement(By.id("flight_form_discount_0")).click();
        driver.findElement(By.id("flight_form_submit")).click();

        assertTrue(driver.getCurrentUrl().startsWith(expectedURL));
    }
}