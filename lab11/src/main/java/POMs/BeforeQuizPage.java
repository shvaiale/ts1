package POMs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class BeforeQuizPage {

    private WebDriver driver;

    @FindBy(xpath = "//button[contains(text(),'Re-attempt quiz')]")
    private WebElement attemptButton;

    @FindBy(id = "id_submitbutton")
    private WebElement submitAttemptButton;

    public BeforeQuizPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void attemptQuiz() {
        attemptButton.click();
        submitAttemptButton.click();
    }
}
