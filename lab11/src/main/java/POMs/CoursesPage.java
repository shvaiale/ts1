package POMs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CoursesPage {

    private WebDriver driver;

    @FindBy(xpath = "//a[contains(text(),'Software Testing')]")
    WebElement TSCourse;

    public CoursesPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver,this);
    }

    public void toTSCourse() {
        TSCourse.click();
    }
}
