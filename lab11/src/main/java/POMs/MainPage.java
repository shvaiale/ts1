package POMs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MainPage {

    private final String url = "https://moodle.fel.cvut.cz";
    private WebDriver driver;

    @FindBy(xpath = "//span[contains(text(),'Log in')]")
    private WebElement logInButton;

    public MainPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void logIn() {
        logInButton.click();
    }

    public void openMainPage() {
        driver.get(url);
    }
}
