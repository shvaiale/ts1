package POMs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class QuizPage {

    private WebDriver driver;

    @FindBy(xpath = "//label[contains(text(),'Answer text Question 1')]/../textarea")
    private WebElement question1;

    @FindBy(xpath = "//span[contains(text(),'Question 2')]/../../span/input")
    private WebElement question2;

    @FindBy(xpath = "//label[contains(text(),'Blank 1 Question 3')]/../select")
    private WebElement question3;

    @FindBy(xpath = "//label[contains(text(),'Blank 1 Question 4')]/../select")
    private WebElement question4;

    @FindBy(xpath = "//a[contains(text(),'Finish attempt ...')]")
    private WebElement finishButton;

    @FindBy(id = "action-menu-toggle")
    private WebElement menu;

    @FindBy(xpath = "//span[@id='actionmenuaction-6']/..")
    private WebElement logoutButton;

    public QuizPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver,this);
    }

    public void answer1() {
        question1.sendKeys("Ales Shvaibovich, cv11(mělo by být)");
    }

    public void answer2() {
        question2.sendKeys(String.valueOf(24 * 60 * 60));
    }

    public void answer3() {
        Select planetSelect = new Select(question3);
        planetSelect.selectByVisibleText("Oberon");
    }

    public void answer4() {
        Select EUSelect = new Select(question4);
        EUSelect.selectByVisibleText("Rumunsko");
    }

    public void answerAll() {
        answer1();
        answer2();
        answer3();
        answer4();
        finishButton.click();
    }

    public void logout() {
        menu.click();
        logoutButton.click();
    }
}
