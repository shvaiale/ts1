package main;

import POMs.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.time.Duration;

public class Main {
    public static void main(String[] args) {
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        driver.manage().window().maximize();

        MainPage mainPage = new MainPage(driver);
        mainPage.openMainPage();
        mainPage.logIn();

        MoodleSignInPage moodleSignInPage = new MoodleSignInPage(driver);
        moodleSignInPage.toSSO();

        SSOGatePage ssoGatePage = new SSOGatePage(driver);
        ssoGatePage.login("login","password");

        CoursesPage coursesPage = new CoursesPage(driver);
        coursesPage.toTSCourse();

        TSPage tsPage = new TSPage(driver);
        tsPage.toTest();

        BeforeQuizPage beforeQuizPage = new BeforeQuizPage(driver);
        beforeQuizPage.attemptQuiz();

        QuizPage quizPage = new QuizPage(driver);
        quizPage.answerAll();

        quizPage.logout();
        LogoutPage logoutPage = new LogoutPage(driver);
        logoutPage.submitLogout();
    }
}
