package POMs;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CoinPage {
    private WebDriver driver;

    @FindBy(xpath = "//button[contains(text(),'Add to Collection')]")
    private WebElement addButton;

    @FindBy(xpath = "//button[contains(text(),'Cancel')]/../button[contains(text(),'Add')]")
    private WebElement confirmAddButton;

    @FindBy(xpath = "//a[contains(text(),'Delete')]")
    private WebElement deleteButton;

    @FindBy(xpath = "//th[contains(text(),'Country')]/../td")
    private WebElement countryText;

    @FindBy(xpath = "//button[contains(text(),'Add coin for wish')]")
    private WebElement addToWishListButton;

    @FindBy(xpath = "//form[@id='wish-form']/div/button[1]")
    private WebElement submitWishList;

    public CoinPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public CoinPage addCoinToCollection() {
        addButton.click();
        confirmAddButton.click();
        return this;
    }

    public CoinPage addCoinToWishList() {
        addToWishListButton.click();
        submitWishList.click();
        return this;
    }

    public CoinPage deleteCoinFromCollection() {
        deleteButton.click();
        Alert alert = driver.switchTo().alert();
        alert.accept();
        return this;
    }

    public boolean isSameCountry(String country) {
        return countryText.getText().equals(country);
    }
}
