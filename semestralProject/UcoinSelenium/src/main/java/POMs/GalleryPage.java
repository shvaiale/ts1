package POMs;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

public class GalleryPage {
    private final static String url = "https://en.ucoin.net/gallery/?uid=275446";
    private WebDriver driver;

    public GalleryPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public GalleryPage openPage() {
        driver.get(url);
        return this;
    }

    public boolean doesContainCoin(String country, String denomination, String year) {
        String searchedStr = String.format("%s\u00A0%s,\u00A0%s", country, denomination, year);

        for (WebElement coin : driver.findElements(By.xpath("//div[@class='coin-desc']/div[1]/a"))) {
            String title = coin.getAttribute("title");
            if (title.equals(searchedStr))
                return true;
        }

        return false;
    }

    public void clickCoin(String country, String denomination, String year) {
        String searchedStr = String.format("%s\u00A0%s,\u00A0%s", country, denomination, year);
        String xpathExpression = String.format("//a[@title='%s']", searchedStr);
        WebElement coin = driver.findElement(By.xpath(xpathExpression));
        coin.click();
    }
}
