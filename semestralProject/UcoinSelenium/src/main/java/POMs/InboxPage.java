package POMs;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class InboxPage {
    private static final String url = "https://en.ucoin.net/messages/?v=inbox";
    private WebDriver driver;

    public InboxPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public InboxPage openPage() {
        driver.get(url);
        return this;
    }

    public boolean gotMessageFrom(String message, String nickname) {
        List<WebElement> inbox = driver.findElements(By.xpath("//td[@class='user-container']"));
        String xpathNick = String.format("//a[@title='%s']", nickname);
        String xpathMassage = String.format("//div[contains(text(),'%s')]", message);
        for (WebElement element : inbox) {
            if (element.findElement(By.xpath(xpathNick)) != null &&
                element.findElement(By.xpath(xpathMassage)) != null)
                return true;
        }

        return false;
    }
}
