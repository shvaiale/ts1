package POMs;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LogInPage {
    private static final String url = "https://en.ucoin.net/login";
    private WebDriver driver;

    @FindBy(xpath = "//label[contains(text(),'E-mail')]/../input")
    private WebElement emailField;

    @FindBy(xpath = "//label[contains(text(),'Password')]/../input")
    private WebElement passwordField;

    @FindBy(xpath = "//button[contains(text(),'Log In')]")
    private WebElement logInButton;

    public LogInPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public LogInPage openPage() {
        driver.get(url);
        return this;
    }

    public LogInPage handleConsent() {
        WebElement consentButton =driver.findElement(By.className("fc-cta-consent"));
        if (consentButton.isDisplayed() && consentButton.isEnabled())
            consentButton.click();
        return this;
    }

    public LogInPage fillEmail(String email) {
        emailField.clear();
        emailField.sendKeys(email);
        return this;
    }

    public LogInPage fillPassword(String password) {
        passwordField.sendKeys(password);
        return this;
    }

    public LogInPage logIn() {
        logInButton.click();
        return this;
    }

    public static String getUrl() {
        return url;
    }
}
