package POMs;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MainPage {
    private final String url = "https://en.ucoin.net";
    private WebDriver driver;

    @FindBy(xpath = "//a[contains(text(), 'Log In')]")
    private WebElement logInButton;

    @FindBy(xpath = "//a[contains(text(),'Sign Out')]")
    private WebElement signOutButton;

    @FindBy(xpath = "//div[@id='avatar-popup']/../div[1]")
    private WebElement popUpLogIn;

    public MainPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public MainPage openPage() {
        driver.get(url);
        return this;
    }

    public MainPage handleConsent() {
        WebElement consentButton =driver.findElement(By.className("fc-cta-consent"));
        if (consentButton.isDisplayed() && consentButton.isEnabled())
            consentButton.click();
        return this;
    }

    public MainPage logIn() {
        popUpLogIn.click();
        logInButton.click();
        return this;
    }

    public MainPage signOut() {
        popUpLogIn.click();
        signOutButton.click();
        return this;
    }
}
