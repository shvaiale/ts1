package POMs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MessagePage {
    private WebDriver driver;

    @FindBy(id = "message")
    private WebElement messageField;

    @FindBy(xpath = "//button[contains(text(),'Send')]")
    private WebElement sendButton;

    public MessagePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver,this);
    }

    public MessagePage writeMessage(String message) {
        messageField.sendKeys(message);
        return this;
    }

    public MessagePage send() {
        sendButton.click();
        return this;
    }
}
