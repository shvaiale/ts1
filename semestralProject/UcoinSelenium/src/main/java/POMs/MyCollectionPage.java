package POMs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

public class MyCollectionPage {
    private static final String url = "https://en.ucoin.net/uid275446?v=home";
    private WebDriver driver;

    @FindBy(xpath = "//button[contains(text(),'Add new coin')]")
    private WebElement buttonAddNewCoin;

    @FindBy(id = "country")
    private WebElement selectCountry;

    @FindBy(id = "year")
    private WebElement selectYear;

    @FindBy(id = "denomination")
    private WebElement selectDenomination;

    @FindBy(xpath = "//div[@id='newcoin']/div[2]/div[2]/div/table/tbody/tr/td/div/a[1]")
    private WebElement firstFoundCoin;

    @FindBy(xpath = "//a[@title='Wish list']")
    private WebElement wishList;

    public MyCollectionPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public MyCollectionPage openPage() {
        driver.get(url);
        return this;
    }

    public MyCollectionPage toWishList() {
        wishList.click();
        return this;
    }

    public MyCollectionPage clickAddCoin() {
        buttonAddNewCoin.click();
        return this;
    }

    public MyCollectionPage selectCountry(String country) {
        Select select = new Select(selectCountry);
        select.selectByVisibleText(country);
        return this;
    }

    public MyCollectionPage selectYear(String year) {
        Select select = new Select(selectYear);
        select.selectByVisibleText(year);
        return this;
    }

    public MyCollectionPage selectDenomination(String denomination) {
        Select select = new Select(selectDenomination);
        select.selectByVisibleText(denomination);
        return this;
    }

    public MyCollectionPage clickFirstFound() {
        firstFoundCoin.click();
        return this;
    }
}