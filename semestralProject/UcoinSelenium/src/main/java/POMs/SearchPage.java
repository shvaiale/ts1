package POMs;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SearchPage {

    private static final String url = "https://en.ucoin.net/catalog";
    private WebDriver driver;

    @FindBy(id = "country-filter")
    private WebElement countryFilter;

    @FindBy(id = "country-filter-dialog")
    private WebElement countryFilterDialog;

    @FindBy(id = "currency-filter")
    private WebElement currencyFilter;

    @FindBy(id = "currency-filter-dialog")
    private WebElement currencyFilterDialog;

    @FindBy(id = "value-filter")
    private WebElement valueFilter;

    @FindBy(id = "value-filter-dialog")
    private WebElement valueFilterDialog;

    @FindBy(id = "year-filter")
    private WebElement yearFilter;

    @FindBy(id = "year-filter-dialog")
    private WebElement yearFilterDialog;

    @FindBy(xpath = "//button[contains(text(),'Find')]")
    private WebElement findButton;

    public SearchPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public SearchPage openPage() {
        driver.get(url);
        return this;
    }

    public SearchPage selectCountry(String country) {
        String xpathExpression = String.format("//div/div/div[@title='%s']", country);
        countryFilter.click();
        countryFilterDialog.findElement(By.xpath(xpathExpression)).click();
        return this;
    }

    public SearchPage selectCurrency(String currency) {
        String xpathExpression = String.format("//div/div/div[@title='%s']", currency);
        currencyFilter.click();
        currencyFilterDialog.findElement(By.xpath(xpathExpression)).click();
        return this;
    }

    public SearchPage selectValue(String value) {
        String xpathExpression = String.format("//div/div/div/span[contains(text(),'%s')]/..", value);
        valueFilter.click();
        valueFilterDialog.findElement(By.xpath(xpathExpression)).click();
        return this;
    }

    public SearchPage selectYear(String year) {
        String xpathExpression = String.format("//div/div/div/span[contains(text(),'%s')]/..", year);
        yearFilter.click();
        yearFilterDialog.findElement(By.xpath(xpathExpression)).click();
        return this;
    }

    public SearchPage find() {
        findButton.click();
        return this;
    }

    public SearchPage clickFirstCoin() {
        driver.findElement(By.xpath("//table[@class='coin']/tbody/tr/td[3]/a")).click();
        return this;
    }
}
