package POMs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SecurityPage {
    private static final String url = "https://en.ucoin.net/settings/?v=security";
    private WebDriver driver;

    @FindBy(id = "newpasswd")
    private WebElement newPasswordField;

    @FindBy(id = "curpasswd")
    private WebElement currentPasswordField;

    @FindBy(xpath = "//button[contains(text(),'Save')]")
    private WebElement saveButton;

    public SecurityPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver,this);
    }

    public SecurityPage openPage() {
        driver.get(url);
        return this;
    }

    public SecurityPage fillCurrentPassword(String password) {
        currentPasswordField.sendKeys(password);
        return this;
    }

    public SecurityPage fillNewPassword(String password) {
        newPasswordField.sendKeys(password);
        return this;
    }

    public SecurityPage save() {
        saveButton.click();
        return this;
    }
}
