package POMs;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class SettingsPage {
    private static final String url = "https://en.ucoin.net/settings";
    private WebDriver driver;

    @FindBy(id = "publicname")
    private WebElement nicknameField;

    @FindBy(id = "country")
    private WebElement countrySelect;

    @FindBy(id = "lang1")
    private WebElement languageSelect;

    @FindBy(xpath = "//button[contains(text(),'Save')]")
    private WebElement saveButton;

    public SettingsPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public SettingsPage openPage() {
        driver.get(url);
        return this;
    }

    public SettingsPage changeNickname(String nickname) {
        nicknameField.clear();
        nicknameField.sendKeys(nickname);
        return this;
    }

    public SettingsPage changeCountry(String country) {
        new Select(countrySelect).selectByVisibleText(country);
        return this;
    }

    public SettingsPage changeLanguage(String language) {
        new Select(languageSelect).selectByVisibleText(language);
        return this;
    }

    public SettingsPage submitChanges() {
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", saveButton);
        saveButton.click();
        return this;
    }
}
