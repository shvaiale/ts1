package POMs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.Set;

public class UserInfoPage {
    private static final String url = "https://en.ucoin.net/uid275446";
    private WebDriver driver;

    @FindBy(xpath = "//div[@class='name-block']/h1")
    private WebElement nicknameText;

    @FindBy(xpath = "//td[contains(text(),'Location')]/../td[2]")
    private WebElement locationText;

    @FindBy(xpath = "//td[contains(text(),'Language')]/../td[2]")
    private WebElement languageText;

    @FindBy(id = "message")
    private WebElement messageButton;

    public UserInfoPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public UserInfoPage openPage() {
        driver.get(url);
        return this;
    }

    public boolean isSameNickname(String nickname) {
        return  nicknameText.getText().equals(nickname);
    }

    public boolean isSameLocation(String location) {
        return  locationText.getText().equals(location);
    }

    public boolean isSameLanguage(String language) {
        return  languageText.getText().equals(language);
    }

    public boolean isSameInfo(String nickname, String location, String language) {
        return  isSameNickname(nickname) &&
                isSameLocation(location) &&
                isSameLanguage(language);
    }

    public UserInfoPage massage() {
        messageButton.click();
        String currentWindowHandle = driver.getWindowHandle();
        Set<String> allWindowHandles = driver.getWindowHandles();

        // Switch to the new tab
        for (String windowHandle : allWindowHandles) {
            if (!windowHandle.equals(currentWindowHandle)) {
                driver.switchTo().window(windowHandle);
                break;
            }
        }
        return this;
    }
}
