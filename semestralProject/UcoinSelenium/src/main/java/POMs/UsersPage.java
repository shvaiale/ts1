package POMs;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class UsersPage {
    private static final String url = "https://en.ucoin.net/users";
    private WebDriver driver;

    @FindBy(id = "search")
    private WebElement searchField;

    @FindBy(xpath = "//button[contains(text(),'Find')]")
    private WebElement findButton;

    public UsersPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public UsersPage openPage() {
        driver.get(url);
        return this;
    }

    public UsersPage fillSearch(String nickname) {
        searchField.sendKeys(nickname);
        return this;
    }

    public UsersPage find() {
        findButton.click();
        return this;
    }

    public UsersPage clickOnUser(String nickname) {
        String xpathExpression = String.format("//a[@title='%s']", nickname);
        driver.findElement(By.xpath(xpathExpression)).click();
        return this;
    }
}
