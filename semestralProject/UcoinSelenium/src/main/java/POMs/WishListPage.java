package POMs;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class WishListPage {
    private WebDriver driver;

    public WishListPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver,this);
    }

    public boolean doesContainCoin(String country, String year) {
        List<WebElement> elements = driver.findElements(By.xpath("//table[@class='wish-coin']/tbody/tr"));
        if (elements.isEmpty()) return false;
        for (WebElement e : elements) {
            String attribute = e.getAttribute("data-tooltip-name");
            if (attribute != null && attribute.contains(country) && attribute.contains(year))
                return true;
        }

        return false;
    }

    public WishListPage deleteCoin(String country, String year) {
        List<WebElement> elements = driver.findElements(By.xpath("//table[@class='wish-coin']/tbody/tr"));
        for (WebElement e : elements) {
            String attribute = e.getAttribute("data-tooltip-name");
            if (attribute.contains(country) && attribute.contains(year)) {
                WebElement deleteButton = e.findElement(By.xpath("//th[last()]/a"));
                Actions actions = new Actions(driver);
                actions.moveToElement(e).perform();
                deleteButton.click();
                Alert alert = driver.switchTo().alert();
                alert.accept();
                return this;
            }
        }

        return this;
    }
}
