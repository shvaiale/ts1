package utils;

import com.opencsv.CSVReader;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class HelpMethods {

    public static List<List<String>> readRecordsFromCSV(String filename) throws Exception {
        List<List<String>> records = new ArrayList<>();
        try (CSVReader csvReader = new CSVReader(new FileReader(filename))) {
            csvReader.readNext();
            String[] values;
            while ((values = csvReader.readNext()) != null)
                records.add(Arrays.asList(values));
        }

        return records;
    }
}
