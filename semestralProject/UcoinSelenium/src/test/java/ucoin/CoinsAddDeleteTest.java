package ucoin;

import POMs.*;
import org.junit.jupiter.api.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import utils.HelpMethods;

import java.time.Duration;
import java.util.List;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class CoinsAddDeleteTest {
    private WebDriver driver;
    private static List<List<String>> records;

    private static String getCountry(List<String> row) {
        return row.get(0);
    }
    private static String getDenomination(List<String> row) {
        return row.get(1);
    }
    private static String getYear(List<String> row) {
        return row.get(2);
    }

    @BeforeAll
    static void loadRecords() throws Exception {
        records = HelpMethods.readRecordsFromCSV(CoinsAddDeleteTest.class.getResource("/importForAdd.csv").getFile());
    }

    @BeforeEach
    void setupDriverAndLogin() {
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        driver.manage().window().maximize();

        LogInPage logInPage = new LogInPage(driver);
        logInPage.openPage().fillEmail("shvaiale@fel.cvut.cz").fillPassword("megakrutoiparol").logIn().handleConsent();
    }

    @Test
    @Order(1)
    void addCoinsFromCSVTest() {
        // Add each coin
        for (List<String> row : records) {
            MyCollectionPage collectionPage = new MyCollectionPage(driver);
            collectionPage.clickAddCoin()
                          .selectCountry(getCountry(row))
                          .selectYear(getYear(row))
                          .selectDenomination(getDenomination(row))
                          .clickFirstFound();

            CoinPage coinPage = new CoinPage(driver);
            coinPage.addCoinToCollection();
            collectionPage.openPage();
        }

        // Check if coins are added to the gallery
        GalleryPage galleryPage = new GalleryPage(driver);
        galleryPage.openPage();
        for (List<String> row : records) {
            boolean result = galleryPage.doesContainCoin(getCountry(row), getDenomination(row), getYear(row));
            Assertions.assertTrue(result);
        }
    }

    @Test
    @Order(2)
    void removeCoinsFromCSVTest() {
        // Delete each coin (were added in the first test)
        GalleryPage galleryPage = new GalleryPage(driver);
        for (List<String> row : records) {
            galleryPage.openPage().clickCoin(getCountry(row), getDenomination(row), getYear(row));
            CoinPage coinPage = new CoinPage(driver);
            coinPage.deleteCoinFromCollection();
        }

        // Check that each coin was deleted from the gallery
        galleryPage.openPage();
        for (List<String> row : records) {
            boolean result = galleryPage.doesContainCoin(getCountry(row), getDenomination(row), getYear(row));
            Assertions.assertFalse(result);
        }
    }
}
