package ucoin;

import POMs.LogInPage;
import POMs.MainPage;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.time.Duration;

public class LogInTest {

    private WebDriver driver;

    @BeforeEach
    public void setupDriver() {
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        driver.manage().window().maximize();
    }

    @Test
    void logInSuccessTest() {
        MainPage mainPage = new MainPage(driver);
        mainPage.openPage().handleConsent().logIn();

        LogInPage logInPage = new LogInPage(driver);
        logInPage.fillEmail("shvaiale@fel.cvut.cz").fillPassword("megakrutoiparol").logIn();

        Assertions.assertNotEquals(LogInPage.getUrl(), driver.getCurrentUrl());
    }

    @Test
    void logInFailTest() {
        MainPage mainPage = new MainPage(driver);
        mainPage.openPage().handleConsent().logIn();

        LogInPage logInPage = new LogInPage(driver);
        logInPage.fillEmail("wrong login").fillPassword("wrong password").logIn();

        Assertions.assertEquals(LogInPage.getUrl(), driver.getCurrentUrl());
    }
}
