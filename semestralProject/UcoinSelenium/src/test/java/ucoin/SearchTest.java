package ucoin;

import POMs.CoinPage;
import POMs.LogInPage;
import POMs.SearchPage;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import java.time.Duration;

public class SearchTest {
    private WebDriver driver;

    @BeforeEach
    void setupDriverAndLogin() {
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        driver.manage().window().maximize();

        LogInPage logInPage = new LogInPage(driver);
        logInPage.openPage().fillEmail("shvaiale@fel.cvut.cz").fillPassword("megakrutoiparol").logIn().handleConsent();
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/importForSearch.csv", numLinesToSkip = 1)
    void searchByCountryCurrencyValueYearTest(String country, String currency, String value, String year) {
        SearchPage searchPage = new SearchPage(driver);
        searchPage.openPage().selectCountry(country)
                             .selectCurrency(currency)
                             .selectValue(value)
                             .selectYear(year)
                             .find().clickFirstCoin();

        CoinPage coinPage = new CoinPage(driver);
        Assertions.assertTrue(coinPage.isSameCountry(country));
    }
}
