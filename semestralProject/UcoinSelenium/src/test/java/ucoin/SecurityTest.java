package ucoin;

import POMs.LogInPage;
import POMs.MainPage;
import POMs.SecurityPage;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.time.Duration;

public class SecurityTest {
    private WebDriver driver;

    @BeforeEach
    void setupDriverAndLogin() {
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        driver.manage().window().maximize();

        LogInPage logInPage = new LogInPage(driver);
        logInPage.openPage().fillEmail("shvaiale@fel.cvut.cz").fillPassword("megakrutoiparol").logIn().handleConsent();
    }

    @Test
    void changePasswordCheck() {
        String oldPassword = "megakrutoiparol";
        String newPassword = "plochoiparol";
        String email = "shvaiale@fel.cvut.cz";

        //Change password
        SecurityPage securityPage = new SecurityPage(driver);
        securityPage.openPage().fillCurrentPassword(oldPassword)
                               .fillNewPassword(newPassword)
                               .save();

        // Sign out
        MainPage mainPage = new MainPage(driver);
        mainPage.openPage().signOut().logIn();

        // Try to log in with old password
        String urlBeforeLogIn = driver.getCurrentUrl();
        LogInPage logInPage = new LogInPage(driver);
        logInPage.fillEmail(email).fillPassword(oldPassword).logIn();

        // Should stay on the same page
        Assertions.assertEquals(urlBeforeLogIn, driver.getCurrentUrl());

        // Try to log in with new password
        logInPage.fillEmail(email).fillPassword(newPassword).logIn();

        // Should go to new page
        Assertions.assertNotEquals(urlBeforeLogIn, driver.getCurrentUrl());

        // Set old password back
        securityPage.openPage().fillCurrentPassword(newPassword)
                .fillNewPassword(oldPassword)
                .save();
    }
}
