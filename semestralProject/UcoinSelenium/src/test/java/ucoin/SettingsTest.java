package ucoin;

import POMs.LogInPage;
import POMs.SettingsPage;
import POMs.UserInfoPage;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.time.Duration;

public class SettingsTest {
    private WebDriver driver;

    @BeforeEach
    void setupDriverAndLogin() {
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        driver.manage().window().maximize();

        LogInPage logInPage = new LogInPage(driver);
        logInPage.openPage().fillEmail("shvaiale@fel.cvut.cz").fillPassword("megakrutoiparol").logIn().handleConsent();
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/importForSettings.csv", numLinesToSkip = 1)
    void changeUserInfoInSettingsTest(String nickname, String country, String language) {
        SettingsPage settingsPage = new SettingsPage(driver);
        settingsPage.openPage().changeNickname(nickname)
                               .changeCountry(country)
                               .changeLanguage(language)
                               .submitChanges();

        UserInfoPage userInfoPage = new UserInfoPage(driver);
        Assertions.assertTrue(userInfoPage.openPage().isSameInfo(nickname, country, language));
    }
}
