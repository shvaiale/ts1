package ucoin;

import POMs.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.time.Duration;

public class UsersInteractionTest {

    private WebDriver driver;

    @BeforeEach
    void setupDriverAndLogin() {
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        driver.manage().window().maximize();

        LogInPage logInPage = new LogInPage(driver);
        logInPage.openPage().fillEmail("shvaiale@fel.cvut.cz").fillPassword("megakrutoiparol").logIn().handleConsent();
    }

    @Test
    void findUserByNicknameTest() {
        String nickname = "tester";
        UsersPage usersPage = new UsersPage(driver);
        usersPage.openPage().fillSearch(nickname).find().clickOnUser(nickname);
        UserInfoPage userInfoPage = new UserInfoPage(driver);

        Assertions.assertTrue(userInfoPage.isSameNickname(nickname));
    }

    @Test
    void messageUserTest() {
        String userUrl = "https://en.ucoin.net/uid276754";
        String sender = "neiron";
        String message = "Hi, tester!";
        driver.get(userUrl);

        // Send message
        UserInfoPage userInfoPage = new UserInfoPage(driver);
        userInfoPage.massage();
        MessagePage messagePage = new MessagePage(driver);
        messagePage.writeMessage(message).send();

        // Log in "tester account"
        MainPage mainPage = new MainPage(driver);
        mainPage.openPage().signOut().logIn();
        LogInPage logInPage = new LogInPage(driver);
        logInPage.fillEmail("neironis94@gmail.com").fillPassword("parolparol").logIn();

        // Check inbox
        InboxPage inboxPage = new InboxPage(driver);
        Assertions.assertTrue(inboxPage.openPage().gotMessageFrom(message,sender));
    }
}
