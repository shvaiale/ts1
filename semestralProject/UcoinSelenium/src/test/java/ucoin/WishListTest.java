package ucoin;

import POMs.*;
import org.junit.jupiter.api.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import utils.HelpMethods;

import java.time.Duration;
import java.util.List;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class WishListTest {
    private WebDriver driver;
    private static List<List<String>> records;

    private static String getCountry(List<String> row) {
        return row.get(0);
    }
    private static String getYear(List<String> row) {
        return row.get(1);
    }

    @BeforeAll
    static void loadRecords() throws Exception {
        records = HelpMethods.readRecordsFromCSV(CoinsAddDeleteTest.class.getResource("/importForWishList.csv").getFile());
    }

    @BeforeEach
    void setupDriverAndLogin() {
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        driver.manage().window().maximize();

        LogInPage logInPage = new LogInPage(driver);
        logInPage.openPage().fillEmail("shvaiale@fel.cvut.cz").fillPassword("megakrutoiparol").logIn().handleConsent();
    }

    @Test
    @Order(1)
    void addToWishListTest() {
        // Add coins to wishlist
        SearchPage searchPage = new SearchPage(driver);
        for (List<String> row : records) {
            searchPage.openPage().selectCountry(getCountry(row))
                                 .selectYear(getYear(row))
                                 .find().clickFirstCoin();
            CoinPage coinPage = new CoinPage(driver);
            coinPage.addCoinToWishList();
        }

        // Check coins are in wishlist
        MyCollectionPage myCollectionPage = new MyCollectionPage(driver);
        myCollectionPage.openPage().toWishList();
        WishListPage wishListPage = new WishListPage(driver);
        for (List<String> row : records) {
            Assertions.assertTrue(wishListPage.doesContainCoin(getCountry(row), getYear(row)));
        }
    }

    @Test
    @Order(2)
    void deleteFromWishListTest() {
        // Delete coins from wishlist
        MyCollectionPage myCollectionPage = new MyCollectionPage(driver);
        myCollectionPage.openPage().toWishList();
        WishListPage wishListPage = new WishListPage(driver);
        for (List<String> row : records) {
            wishListPage.deleteCoin(getCountry(row), getYear(row));
        }

        // Check coins are not in wishlist
        for (List<String> row : records) {
            Assertions.assertFalse(wishListPage.doesContainCoin(getCountry(row), getYear(row)));
        }
    }
}
