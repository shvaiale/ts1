package POMs;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class AdvancedSearchPage {
    private WebDriver driver;

    @FindBy(id = "all-words")
    private WebElement allOfField;

    @FindBy(id = "least-words")
    private WebElement oneOfField;

    @FindBy(id = "date-facet-mode")
    private WebElement selectYearOption;

    @FindBy(id = "facet-start-year")
    private WebElement yearField;

    @FindBy(id = "submit-advanced-search")
    private WebElement searchButton;

    public AdvancedSearchPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void fillAllOf(String allOf) {
        allOfField.sendKeys(allOf);
    }

    public void fillOneOf(String oneOf) {
        oneOfField.sendKeys(oneOf);
    }

    public void fillInYear(String year) {
        new Select(selectYearOption).selectByIndex(1);
        yearField.sendKeys(year);
    }

    public void search() {
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", searchButton);
        searchButton.click();
    }
}
