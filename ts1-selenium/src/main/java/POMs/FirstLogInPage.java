package POMs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FirstLogInPage {
    private WebDriver driver;

    @FindBy(id = "login-email")
    private WebElement emailField;

    @FindBy(id = "email-submit")
    private WebElement continueButton;

    public FirstLogInPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void fillEmail(String email) {
        emailField.sendKeys(email);
    }

    public void clickContinue() {
        continueButton.click();
    }
}
