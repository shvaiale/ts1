package POMs;

import main.ArticleInfo;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.ArrayList;
import java.util.List;

public class SearchPage {
    private WebDriver driver;

    @FindBy(xpath = "//span[contains(text(),'Article')]")
    private WebElement articleOption;

    @FindBy(id = "date-from")
    private WebElement yearFrom;

    @FindBy(id = "date-to")
    private WebElement yearTo;

    @FindBy(id = "search-submit")
    private WebElement searchButton;

    @FindBy(className = "app-search__button-filter")
    private WebElement filterButton;

    @FindBy(xpath = "//button[contains(text(),'Update results')]")
    private WebElement submitFilterButton;

    @FindBy(css = "ol[data-test='darwin-search']")
    private WebElement articlesElement;

    @FindBy(id = "search-springerlink")
    private WebElement searchField;

    public SearchPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver,this);
    }

    public void filterArticles() {
        if (filterButton.isDisplayed()) filterButton.click();
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", articleOption);
        articleOption.click();
        submitFilterButton.click();
    }

    public void filterThisYear() {
        if (filterButton.isDisplayed()) filterButton.click();
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", yearFrom);
        yearFrom.sendKeys("2024");
        yearTo.sendKeys("2024");
        submitFilterButton.click();
    }

    public List<ArticleInfo> getArticleInfos(int count) {
        List<WebElement> articles = articlesElement.findElements(By.cssSelector("li[data-test='search-result-item']"));
        List<ArticleInfo> infos = new ArrayList<>();
        for (int i = 0; i < count && i < articles.size(); i++) {
            WebElement article = articles.get(i);
            infos.add(new ArticleInfo(article));
        }

        return infos;
    }

    public void search() {
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", searchButton);
        searchButton.click();
    }

    public void search(String title) {
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", searchButton);
        searchField.sendKeys(title);
        searchButton.click();
    }
}
