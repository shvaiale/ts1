package POMs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SecondLogInPage {
    private WebDriver driver;

    @FindBy(id = "login-password")
    private WebElement passwordField;

    @FindBy(id = "password-submit")
    private WebElement continueButton;

    public SecondLogInPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver,this);
    }

    public void fillPassword(String password) {
        passwordField.sendKeys(password);
    }

    public void clickContinue() {
        continueButton.click();
    }
}
