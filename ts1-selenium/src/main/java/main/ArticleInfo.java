package main;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class ArticleInfo {
    private String DOI;
    private String title;
    private String publishedDate;

    public ArticleInfo(WebElement article) {
        this.title = article.findElement(By.cssSelector("h3[data-test='title'] a")).getText();
        this.DOI = article.findElement(By.cssSelector("h3[data-test='title'] a")).getAttribute("href").substring(34);
        this.publishedDate = article.findElement(By.cssSelector("span[data-test='published']")).getText();
    }

    public ArticleInfo(String DOI, String title, String publishedDate) {
        this.DOI = DOI;
        this.title = title;
        this.publishedDate = publishedDate;
    }

    public String getDOI() {
        return DOI;
    }

    public String getTitle() {
        return title;
    }

    public String getPublishedDate() {
        return publishedDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ArticleInfo that = (ArticleInfo) o;

        if (!DOI.equals(that.DOI)) return false;
        if (!title.equals(that.title)) return false;
        return publishedDate.equals(that.publishedDate);
    }

    @Override
    public int hashCode() {
        int result = DOI.hashCode();
        result = 31 * result + title.hashCode();
        result = 31 * result + publishedDate.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "ArticleInfo{" +
                "DOI='" + DOI + '\'' +
                ", title='" + title + '\'' +
                ", publishedDate='" + publishedDate + '\'' +
                '}';
    }
}
