package main;

import POMs.AdvancedSearchPage;
import POMs.SearchPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.time.Duration;

public class Main {

    // Just code on which I was training in Selenium
    public static void main(String[] args) {
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        driver.manage().window().maximize();
//        driver.get("https://link.springer.com");
        driver.get("https://link.springer.com/advanced-search");

        // Handle cookies
        WebElement acceptButton = driver.findElement(By.className("cc-banner__button-accept"));
        if (acceptButton.isDisplayed() && acceptButton.isEnabled())
            acceptButton.click();

        AdvancedSearchPage advancedSearchPage = new AdvancedSearchPage(driver);
        advancedSearchPage.fillAllOf("Page Object Model");
        advancedSearchPage.fillOneOf("Sellenium Testing");
        advancedSearchPage.fillInYear("2024");
        advancedSearchPage.search();

//        // Handle cookies
//        acceptButton = driver.findElement(By.className("cc-banner__button-accept"));
//        if (acceptButton.isDisplayed() && acceptButton.isEnabled())
//            acceptButton.click();

        SearchPage searchPage = new SearchPage(driver);
        searchPage.filterArticles();
        searchPage.filterThisYear();
        for (ArticleInfo info : searchPage.getArticleInfos(4))
            System.out.println(info);

//        // main.Main page -> login page
//        MainPage mainPage = new MainPage(driver);
//        mainPage.clickLogInButton();

//        // Handle cookies
//        acceptButton = driver.findElement(By.className("cc-banner__button-accept"));
//        if (acceptButton.isDisplayed() && acceptButton.isEnabled())
//            acceptButton.click();

//        // Login
//        FirstLogInPage firstLogInPage = new FirstLogInPage(driver);
//        firstLogInPage.fillEmail("shvaiale@fel.cvut.cz");
//        firstLogInPage.clickContinue();
//        SecondLogInPage secondLogInPage = new SecondLogInPage(driver);
//        secondLogInPage.fillPassword("megakrutoiparol");
//        secondLogInPage.clickContinue();
    }
}
