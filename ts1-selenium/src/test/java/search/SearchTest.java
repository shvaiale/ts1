package search;

import POMs.*;
import main.ArticleInfo;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.time.Duration;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SearchTest {
    private static WebDriver driver;

    private static void handleCookies() {
        WebElement acceptButton = driver.findElement(By.className("cc-banner__button-accept"));
        if (acceptButton.isDisplayed() && acceptButton.isEnabled())
            acceptButton.click();
    }

    private static void setDriver() {
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        driver.manage().window().maximize();
    }

    private static List<ArticleInfo> getInfosToMatch() {
        setDriver();
        driver.get("https://link.springer.com/advanced-search");
        handleCookies();

        AdvancedSearchPage advancedSearchPage = new AdvancedSearchPage(driver);
        advancedSearchPage.fillAllOf("Page Object Model");
        advancedSearchPage.fillOneOf("Sellenium Testing");
        advancedSearchPage.fillInYear("2024");
        advancedSearchPage.search();

        SearchPage searchPage = new SearchPage(driver);
        searchPage.filterArticles();
        searchPage.filterThisYear();
        return searchPage.getArticleInfos(4);
    }

    @Test
    void loginTest() {
        setDriver();
        driver.get("https://link.springer.com");
        handleCookies();

        // Main page -> login page
        MainPage mainPage = new MainPage(driver);
        mainPage.clickLogInButton();
        handleCookies();

        // Login
        FirstLogInPage firstLogInPage = new FirstLogInPage(driver);
        firstLogInPage.fillEmail("shvaiale@fel.cvut.cz");
        firstLogInPage.clickContinue();
        SecondLogInPage secondLogInPage = new SecondLogInPage(driver);
        secondLogInPage.fillPassword("megakrutoiparol");
        secondLogInPage.clickContinue();

        // If logging in succeed, "Account" is written on the widget instead of "Log in"
        assertEquals("Account", mainPage.getLoginWidgetText());
    }

    @ParameterizedTest
    @MethodSource("getInfosToMatch")
    void searchByTitleTest(ArticleInfo articleInfo) {
        driver.get("https://link.springer.com/search");

        // Search for the given article
        SearchPage searchPage = new SearchPage(driver);
        searchPage.search(articleInfo.getTitle());
        ArticleInfo info = searchPage.getArticleInfos(1).get(0);

        assertEquals(articleInfo, info);
    }
}
